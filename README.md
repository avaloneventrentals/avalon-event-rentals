Avalon is a full-service tent and event rental company. Avalon was formed with the objective of realizing all your luxury and bespoke event needs, catering to individuals, brands and companies.

Address: 10803 Warwana Rd, Suite C, Houston, TX 77043, USA

Phone: 713-974-3646
